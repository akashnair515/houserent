/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.house.rental.managment.service;

import in.ac.gpckasaragod.house.rental.managment.ui.CustomerDetailForm;
import java.util.List;

/**
 *
 * @author student
 */
public interface CustomerDetailsService {
    public String saveCustomer(String Name,String Address);
    public CustomerDetailsService readCustomer(Integer Id);
    public List<CustomerDetailForm> getAllCustomer();
    public String updateCustomer(Integer Id,String Name,String Address);
    public String deleteCustomer(Integer Id);
    
}
