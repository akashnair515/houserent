/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.house.rental.managment.service.impl;

import in.ac.gpckasaragod.house.rental.managment.model.ui.data.CustomerDetails;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class CustomerDetailServiceImpl extends ConnectionServiceImpl implements CustomerDetailService{
    public String CustomerDetails(Integer Id, String Name, String Address) {
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO CUSTOMER_DETAILS (ID,NAME,ADDRESS) VALUES " 
                    + "('"+Id+ "',"+Name+ ",'"+Address+"')";
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status!=1){
                return "Save failed";
            }else{
                return "Saved successfully";
            }
        }catch (SQLException ex) {
            Logger.getLogger(CustomerDetailServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
            return "Save failed";
        }
}
    public CustomerDetails readCustomerDetails(Integer Id){
        CustomerDetails customerDetails = null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM CUSTOMER_DETAILS WHERE ID="+Id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                Integer id = resultSet.getInt("ID");
                String name = resultSet.getString("NAME");
                String address = resultSet.getString("ADDRESS");
                customerDetails = new CustomerDetails(id,name,address);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDetailServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return customerDetails;
       
    }
     public List<CustomerDetails> getAllCustomerDetails(){
        List<CustomerDetails> customerdetails = new ArrayList<>();
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT CUSTOMER_DETAILS.ID,NAME,ADDRESS, FROM CUSTOMER_DETAILS JOIN CUSTOMER ON CUSTOMER.ID=HOUSE_DETAILS.ID";
        ResultSet resultSet = statement.executeQuery(query);
        
        while(resultSet.next()){
             Integer id = resultSet.getInt("ID");
                String name = resultSet.getString("NAME");
                String address = resultSet.getString("ADDRESS");
                CustomerDetails customerDetails = new CustomerDetails(id,name,address);
                customerdetails.add(customerDetails);
        }
        
    }   catch (SQLException ex) {
            Logger.getLogger(CustomerDetailServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
    }
        return customerdetails;
    
    }
     
     public String updateCustomerDetails(Integer id,String name,String address){
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "UPDATE CUSTOMER_DETAILS SET ID='"+id+"',NAME="+name+",ADDRESS='"+address+"',WHERE ID="+id;
        System.out.print(query);
        int update = statement.executeUpdate(query);
        if(update !=1)
            return "Update failed";
        else
            return "Updated successfully";
        }catch(SQLException ex){
            return "Update failed";
        }
    }
     public String deleteCustomerDetails(Integer id) {
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM CUSTOMER_DETAILS WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else
                return "Deleted successfully";
            
        } catch (SQLException ex) {
           return "Delete failed";
        }
     
}

}


 
    

