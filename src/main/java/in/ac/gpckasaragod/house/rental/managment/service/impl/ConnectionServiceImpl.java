/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.house.rental.managment.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class ConnectionServiceImpl {
    String jdbcUrl = "jdbc:mysql://localhost:3306/";
    String databaseName = "House-Rental-Managment";
    String connectionString = jdbcUrl+databaseName;
    String username = "root";
    String password ="mysql";
    public Connection getConnection() {
        try {
            return DriverManager.getConnection( connectionString,username,password);
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
