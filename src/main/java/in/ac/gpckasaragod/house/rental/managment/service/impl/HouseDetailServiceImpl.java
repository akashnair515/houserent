/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.house.rental.managment.service.impl;

import in.ac.gpckasaragod.house.rental.managment.model.ui.data.HouseDetails;
import in.ac.gpckasaragod.house.rental.managment.service.HouseDetailsService;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public  class HouseDetailServiceImpl extends ConnectionServiceImpl implements HouseDetailsService{

    @Override
    public String saveHouseDetails(Integer Id, Integer customerId, Integer floorNo, Integer bedroomNo, Integer bathroomNo) {
       try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement(); 
            String query = "INSERT INTO HOUSE_DETAILS (CUSTOMER_ID,FLOOR_NO,BEDROOM_NO,BATHROOM_NO) VALUES " 
                    + "('"+customerId+ "','" +floorNo+ "','" +bedroomNo+"','" +bathroomNo+"')" ;
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status!=1){
                return "Save failed";
            }else{
                return "Saved successfully";
            }
        }catch (SQLException ex) {
            Logger.getLogger(HouseDetailServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
            return "Save failed";
        }
    }

    @Override
    public HouseDetails readHouseDetails(Integer Id,Integer  customerId,Integer floorNo,Integer fedroomno,Integer bathroomNo){
        HouseDetails housedetails = null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM HOUSE_DETAILS WHERE ID="+Id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                Integer id = resultSet.getInt("ID");
                Integer CustomerId = resultSet.getInt("CUSTOMER_ID");
               Integer FloorNo = resultSet.getInt("FLOOR_NO");
                Integer BedroomNo = resultSet.getInt("BEDROOM_NO");
                int BathroomNo = resultSet.getInt("BATHROOM_NO");
                housedetails = new HouseDetails(id,CustomerId,FloorNo,BedroomNo,BathroomNo);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(HouseDetailServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return housedetails;
       
    }

    /**
     *
     * @return
     */
    @Override
    public List<HouseDetails> getAllHouseDetails(){
        
        List<HouseDetails> housedetails = new ArrayList<>();
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT * FROM HOUSE_DETAILS";
        ResultSet resultSet = statement.executeQuery(query);
        
        while(resultSet.next()){
             Integer id = resultSet.getInt("ID");
                Integer customerId = resultSet.getInt("CUSTOMER_ID");
               Integer floorNo = resultSet.getInt("FLOOR_NO");
                Integer bedroomNo = resultSet.getInt("BEDROOM_NO");
                int bathroomNo = resultSet.getInt("BATHROOM_NO");
                housedetails = (List<HouseDetails>) new HouseDetails(id,customerId,floorNo,bedroomNo,bathroomNo);
                housedetails.add((HouseDetails) housedetails);
        }
        
    }   catch (SQLException ex) {
            Logger.getLogger(HouseDetailServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
    }
        return housedetails;
    
    }
    
   

    @Override
    public String deleteHouseDetails(Integer id) {
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM HOUSE_DETAILS WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else
                return "Deleted successfully";
            
        } catch (SQLException ex) {
           return "Delete failed";
        }
       
       
    }

   

    @Override
    public String updateHouseDetails(Integer Id, Integer customerId, Integer floorNo, Integer fedroomno, Integer bathroomNo) {
         try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
            Integer bathroom = null;
            Integer bedroomno = null;
            Integer customerid = null;
            Integer floorno = null;
        String query = "UPDATE HOUSE_DETAILS SET CUSTOMER_ID='"+customerid+"',FLOOR_NO='"+floorno+"',BEDROOM_NO='"+bedroomno+"',BATHROOM_NO='"+bathroom+"' WHERE ID="+Id;
        System.out.print(query);
        int update = statement.executeUpdate(query);
        if(update !=1)
            return "Update failed";
        else
            return "Updated successfully";
        }catch(SQLException ex){
            return "Update failed";
        }
    }
    
                
}

    
    

