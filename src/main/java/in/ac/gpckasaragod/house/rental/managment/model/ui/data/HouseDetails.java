/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.house.rental.managment.model.ui.data;

import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class HouseDetails {
   private Integer id;
   private Integer customerId;
   private Integer floorNo;
   private Integer bedroomNo;
   private Integer bathroomNo;

    public HouseDetails(Integer id, Integer customerId, Integer floorNo, Integer bedroomNo, Integer bathroomNo) {
        this.id = id;
        this.customerId = customerId;
        this.floorNo = floorNo;
        this.bedroomNo = bedroomNo;
        this.bathroomNo = bathroomNo;
    }
    private static final Logger LOG = Logger.getLogger(HouseDetails.class.getName());

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getFloorNo() {
        return floorNo;
    }

    public void setFloorNo(Integer floorNo) {
        this.floorNo = floorNo;
    }

    public Integer getBedroomNo() {
        return bedroomNo;
    }

    public void setBedroomNo(Integer bedroomNo) {
        this.bedroomNo = bedroomNo;
    }

    public Integer getBathroomNo() {
        return bathroomNo;
    }

    public void setBathroomNo(Integer bathroomNo) {
        this.bathroomNo = bathroomNo;
    }
}