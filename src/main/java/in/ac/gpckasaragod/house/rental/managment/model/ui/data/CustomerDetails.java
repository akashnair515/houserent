/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.house.rental.managment.model.ui.data;

import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class CustomerDetails {
    private Integer Id;
    private String Name;
    private String Address;
    
    public CustomerDetails(Integer Id, String Name, String Address) {
        this.Id = Id;
        this.Name = Name;
        this.Address = Address;
    }
    private static final Logger LOG = Logger.getLogger(CustomerDetails.class.getName());

    public Integer getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    public String getAddress() {
        return Address;
    }

    public static Logger getLOG() {
        return LOG;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }
    
    
}
