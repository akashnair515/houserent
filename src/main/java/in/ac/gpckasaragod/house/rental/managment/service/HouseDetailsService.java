/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.house.rental.managment.service;

import in.ac.gpckasaragod.house.rental.managment.model.ui.data.HouseDetails;
import java.util.List;

/**
 *
 * @author student
 */
public interface HouseDetailsService {
     public String saveHouseDetails(Integer Id, Integer customerId, Integer floorNo, Integer bedroomNo, Integer bathroomNo);
   public HouseDetails readHouseDetails(Integer Id,Integer  customerId,Integer floorNo,Integer fedroomno,Integer bathroomNo);
   public List<HouseDetails> getAllHouseDetails();
    public String updateHouseDetails(Integer Id,Integer  customerId,Integer floorNo,Integer fedroomno,Integer bathroomNo);
    public String deleteHouseDetails(Integer Id);
    
}
